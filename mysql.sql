show databases;

create table users (
	user_id int auto_increment primary key,
	username varchar(40),
	password varchar(255),
	email varchar(255)
);

-- alter database binar rename to bej1;
alter table users add type_id int;
alter table users add foreign key(type_id) references user_type(type_id);
alter table users drop column type_id;

-- drop table users;
-- drop database binar;

insert into users(username, password, email)
	values 
	('ade', 'ade', 'ade@email.com'),
	('hermansyah', 'hermansyah', 'hermansyah@email.com');	

	
select * from users;
select * from users where username = 'rizkyfauzi';

-- drop, update, delete

update users set username = 'fauzifauzi', email = 'fauzi@email.com' where email = 'rizky@email.com';
update users set username = 'bej1-nich';

update users set type_id = 1 where username in ('ade', 'hermansyah');
update users set type_id = 2 where username = 'rizkyfauzi';

-- delete from users;
-- delete from users where email = 'bej1@email.com';
-- delete from users where username is null;

truncate table users;

alter table users auto_increment = 1;


-- user_type
create table user_type (
	type_id int auto_increment primary key,
	type_name varchar(50)
);

insert user_type(type_name)
	values('vip'),
	('reguler');
	
select * from user_type;

select u.username as nama as tipe_user from users u 
	join user_type ut on ut.type_id = u.type_id 
	where ut.type_name = 'vip';

select u.username, u.email from users u 
	where u.type_id = (select ut.type_id from user_type ut where ut.type_name = 'vip');
	
-- ambil username, email dan tipe_user tapi type user nya vip
select u.username, u.email from users u where u.type_id = 1;
select u.username, u.email, ut.type_name from users u 
	join user_type ut on ut.type_id = u.type_id where u.type_id = 1;


	
select username, email, 'kampus' as kampoes, 1 from users;
select distinct(type_id) from users;
select sum(type_id) from users;
select max(type_id) from users;
select min(type_id) from users;
select count(*) from users;
select count(u.username) from users u;
select count(1) from users u;

select 

select * from user_type;