package org.binar.chapter4;

import org.binar.chapter4.model.Users;
import org.binar.chapter4.repository.UsersRepository;
import org.binar.chapter4.service.UserTypeService;
import org.binar.chapter4.service.UsersService;
import org.binar.chapter4.service.UsersServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class MockTests {

    @Mock
    private UsersRepository usersRepository;

    @Mock
    private UserTypeService userTypeService;

    private UsersService usersService;

    @BeforeEach
    void init() {
        this.usersService = new UsersServiceImpl(usersRepository, userTypeService);
    }

    @Test
    void testMock() {
        Users expected = new Users();
        expected.setUsername("admin");
        UsersService usersService1 = Mockito.spy(new UsersServiceImpl(usersRepository, userTypeService));
        Mockito.when(usersRepository.findByUsername("admin"))
                .thenReturn(new Users("admin", "admin@email.com", "test"));
        Assertions.assertEquals(usersService1.getUsersByUsername("admin").getUsername(), expected.getUsername());
    }

    @Test
    void testSpy() {
        List<String> nama = new ArrayList<>();
        List<String> namaSpy = Mockito.spy(nama);
        List<String> namaMock = Mockito.mock(nama.getClass());

        namaSpy.add("Rizky");
        namaSpy.add("Fauzi");

        String[] namas = {"Rizky", "Fauzi"};
        Mockito.when(namaSpy.get(0)).thenReturn("UHUY");

        for(int i = 0; i < namaSpy.size(); i++) {
            System.out.println(namaSpy.get(i));
        }
        Assertions.assertArrayEquals(namas, namaSpy.toArray());

        namaMock.add("Rizky Mock");
        Mockito.when(namaMock.get(0)).thenReturn("UHUY MOCK");
        System.out.println(namaMock.get(0));
    }
}
