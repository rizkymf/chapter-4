package org.binar.chapter4;

import org.binar.chapter4.model.UserType;
import org.binar.chapter4.model.Users;
import org.binar.chapter4.repository.UserTypeRepository;
import org.binar.chapter4.repository.UsersRepository;
import org.binar.chapter4.service.UserTypeService;
import org.binar.chapter4.service.UsersService;
import org.binar.chapter4.service.UsersServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class MockitoTest {

    @Mock
    private UsersRepository usersRepository;

    @Mock
    private UserTypeRepository userTypeRepository;

    @Mock
    private UserTypeService userTypeService;

    private UsersService usersService;

    @BeforeEach
    void init() {
        MockitoAnnotations.openMocks(this);
        this.usersService = new UsersServiceImpl(usersRepository, userTypeService);
    }

    @Test
    void testFindUsers() {
        usersService.getAllUsers();
        Mockito.verify(usersRepository, Mockito.times(1)).findAll();
    }

    @Test
    void testAddUsersWithNewType() {
        Users users = new Users("test1", "test1@email.com", "test1");

        usersService.saveUsers(users.getUsername(), users.getPassword(), users.getEmail(), "VIP");
        Mockito.verify(userTypeService, Mockito.times(2)).findByTypeName("VIP");
        Mockito.verify(userTypeService, Mockito.times(1)).saveUserType("VIP");
    }

    @Test
    void testAddUsersWithExistingType() {
        Users users = new Users("test1", "test1@email.com", "test1");
        UserType userType = new UserType();
        userType.setTypeName("Regular");
        Mockito.when(userTypeService.findByTypeName("Regular")).thenReturn(userType);
        usersService.saveUsers(users.getUsername(), users.getPassword(), users.getEmail(), "Regular");

        Mockito.verify(this.userTypeService, Mockito.times(1)).findByTypeName("Regular");
    }

    @Test
    void testSpy() {
        List<String> list = new ArrayList<>();
        List<String> spyList = Mockito.spy(list);

        spyList.add("A");
        spyList.add("B");

        Mockito.verify(spyList).add("A");
        Mockito.verify(spyList).add("B");

        assertEquals(2, spyList.size());
    }

    @Test
    void testStub() {
        UsersService usersService1 = Mockito.spy(new UsersServiceImpl(this.usersRepository, this.userTypeService));
        List<Users> users = new ArrayList<>();

        Mockito.when(usersService1.getAllUsers()).thenReturn(users);

        assertEquals(usersService1.getAllUsers(), users);
    }

    @Test
    void testGetUsersByTypeName() {
        UsersService usersService1 = Mockito.spy(new UsersServiceImpl(this.usersRepository,
                this.userTypeService));
        Mockito.when(usersRepository.findByTypeName("NONE")).thenReturn(null);
        Assertions.assertThrows(NullPointerException.class,
                () -> usersService1.getUsersByTypeName("NONE"), "Null nih tapi boong");
    }

    @Test
    void cobacoba() {
        UsersServiceImpl u = new UsersServiceImpl(this.usersRepository, this.userTypeService);
        List<Integer> a = u.coba("1", "2", "3", "14");
        u.coba();
        u.coba("23");
        a.forEach(v -> {
            System.out.println(v);
        });
    }

}
