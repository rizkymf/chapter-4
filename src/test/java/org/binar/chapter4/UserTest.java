package org.binar.chapter4;

import org.binar.chapter4.controller.UsersController;
import org.binar.chapter4.enumeration.ERole;
import org.binar.chapter4.model.Roles;
import org.binar.chapter4.model.Users;
import org.binar.chapter4.service.ReportsService;
import org.binar.chapter4.service.UsersServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@SpringBootTest
class UserTest {

    @Autowired
    private UsersController usersController;

    @Autowired
    private ReportsService reportsService;

    @Test
    @DisplayName("Test GET User")
    void getUser() {
        Users user = new Users();
        user.setUserId(2);
        user.setUsername("customer");
        user.setEmail("customer@email.com");
        user.setPassword("$2a$10$.3KWGFijCWI7vhzhWxV3iuIhnFp0dje5htRKEMlICZyZXUiE3.6W2");
        Set<Roles> roles = new HashSet<>();
        roles.add(new Roles(1, ERole.CUSTOMER));
        user.setRoles(roles);

        MultiValueMap<String, String> headers = new HttpHeaders();
        headers.put("Binar", Arrays.asList("BEJ1"));
        headers.set("Keep-Alive", "timeout=6000000");

        assertThat(new ResponseEntity<>(user, headers, HttpStatus.ACCEPTED))
                .usingRecursiveComparison().isEqualTo(usersController.getUser(2));
    }

    @Test
    void getUserByUsernameAndEmail() {
        Assertions.assertEquals("admin",
                usersController.getUserByUsernameAndEmail("admin", "admin@email.com")
                        .getUsername());
    }

    @Test
    void updateEmail() {
        Assertions.assertThrows(NullPointerException.class,
                () -> usersController.updateUser("EHE", "EHE@email.com"));
    }

}
