package org.binar.chapter4;

import org.binar.chapter4.model.Users;
import org.binar.chapter4.service.TransactionalHandson;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class TransactionalTests {

    @Autowired
    TransactionalHandson transactionalHandson;

    Users users;

    @BeforeEach
    void init() {
        users = new Users();
        users.setUserId(99);
        users.setUsername("testTrx");
        users.setEmail("trx@trx.com");
        users.setPassword("transactional");
    }

    @Test
    void testTransaction(){

        transactionalHandson.insertNewUser(users);
    }

    @Test
    void testTrxWoAnnotation() {
        transactionalHandson.insertNewUserWoTrx(users);
    }


}
