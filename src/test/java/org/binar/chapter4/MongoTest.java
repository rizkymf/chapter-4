//package org.binar.chapter4;
//
//import org.binar.chapter4.model.Book;
//import org.binar.chapter4.service.BookServiceImpl;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//
//import java.util.List;
//
//@SpringBootTest
//public class MongoTest {
//
//    @Autowired
//    BookServiceImpl bookService;
//
//    @Test
//    void testSaveBook() {
//        bookService.saveBook(1, "TESTT", "TESTTT", 75000.0d);
//    }
//
//    @Test
//    void testGetBookByAuthor() {
//        List<Book> books = bookService.findBookByAuthor("rizky");
//        System.out.println("---------------------------");
//        books.forEach(val -> {
//            System.out.println("Author : " + val.getBookAuthor());
//            System.out.println("Book Name : " + val.getBookName());
//            System.out.println("Book Cost : Rp." + val.getBookCost());
//            System.out.println("---------------------------");
//        });
//    }
//
//    @Test
//    void testGetBookByBookCost() {
//        List<Book> books = bookService.findBookByBookCostLessThan(46000.0);
//        System.out.println("---------------------------");
//        books.forEach(val -> {
//            System.out.println("Author : " + val.getBookAuthor());
//            System.out.println("Book Name : " + val.getBookName());
//            System.out.println("Book Cost : Rp." + val.getBookCost());
//            System.out.println("---------------------------");
//        });
//    }
//}
