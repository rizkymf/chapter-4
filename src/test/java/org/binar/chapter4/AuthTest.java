package org.binar.chapter4;

import org.aspectj.lang.annotation.Before;
import org.binar.chapter4.config.JwtUtils;
import org.binar.chapter4.controller.AuthController;
import org.binar.chapter4.enumeration.ERole;
import org.binar.chapter4.model.JwtResponse;
import org.binar.chapter4.model.Roles;
import org.binar.chapter4.model.SignupRequest;
import org.binar.chapter4.model.UserDetailsImpl;
import org.binar.chapter4.repository.RoleRepository;
import org.binar.chapter4.repository.UsersRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.*;

@ExtendWith(MockitoExtension.class)
class AuthTest {

    private AuthController authController;

    @Mock
    private UsersRepository usersRepository;

    @Spy
    private AuthenticationManager authenticationManager;

    @Mock
    private RoleRepository roleRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private JwtUtils jwtUtils;

    @Mock
    Authentication authentication;

    @BeforeEach
    void init() {
        MockitoAnnotations.openMocks(this);
        authController = new AuthController(this.authenticationManager, this.usersRepository,
                this.jwtUtils, this.roleRepository, this.passwordEncoder);
    }

    @Test
    void testSignup() {
        Set<String> roles = new HashSet<>();
        roles.add("ADMIN");
        SignupRequest signupRequest = new SignupRequest();
        signupRequest.setUsername("admin");
        signupRequest.setEmail("admin@email.com");
        signupRequest.setPassword("admin");
        signupRequest.setRole(roles);

        Mockito.when(usersRepository.existsByUsername("admin")).thenReturn(Boolean.FALSE);
        Mockito.when(usersRepository.existsByEmail("admin@email.com")).thenReturn(Boolean.FALSE);
        Mockito.when(roleRepository.findByName(ERole.ADMIN))
                .thenReturn(Optional.of(new Roles(1, ERole.ADMIN)));

        Assertions.assertEquals("User registered successfully",
                authController.registerUser(signupRequest).getBody().getMessage());
    }

    @Test
    void signUpUsernameExists() {
        Set<String> roles = new HashSet<>();
        roles.add("ADMIN");
        SignupRequest signupRequest = new SignupRequest();
        signupRequest.setUsername("admin");
        signupRequest.setEmail("admin@email.com");
        signupRequest.setPassword("admin");
        signupRequest.setRole(roles);

        Mockito.when(usersRepository.existsByUsername("admin")).thenReturn(Boolean.TRUE);

        Assertions.assertEquals("Error: Username is already taken!",
                authController.registerUser(signupRequest).getBody().getMessage());
    }

    @Test
    void signUpEmailExists() {
        Set<String> roles = new HashSet<>();
        roles.add("ADMIN");
        SignupRequest signupRequest = new SignupRequest();
        signupRequest.setUsername("admin");
        signupRequest.setEmail("admin@email.com");
        signupRequest.setPassword("admin");
        signupRequest.setRole(roles);

        Mockito.when(usersRepository.existsByUsername("admin")).thenReturn(Boolean.FALSE);
        Mockito.when(usersRepository.existsByEmail("admin@email.com")).thenReturn(Boolean.TRUE);

        Assertions.assertEquals("Error: Email is already taken!",
                authController.registerUser(signupRequest).getBody().getMessage());
    }

    @Test
    void signupRoleNull() {
        SignupRequest signupRequest = new SignupRequest();
        signupRequest.setUsername("admin");
        signupRequest.setEmail("admin@email.com");
        signupRequest.setPassword("admin");

        Mockito.when(usersRepository.existsByUsername("admin")).thenReturn(Boolean.FALSE);
        Mockito.when(usersRepository.existsByEmail("admin@email.com")).thenReturn(Boolean.FALSE);
        Mockito.when(roleRepository.findByName(ERole.CUSTOMER))
                .thenReturn(Optional.of(new Roles(1, ERole.CUSTOMER)));

        Assertions.assertEquals("User registered successfully",
                authController.registerUser(signupRequest).getBody().getMessage());
    }

    @Test
    void testSignin() {
        Map<String, Object> login = new HashMap<>();
        login.put("username", "admin");
        login.put("password", "admin");

        // TODO: apapun lah
        Mockito.when(jwtUtils.generateJwtToken(authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(login.get("username"), login.get("password"))
        ))).thenReturn("ini token");

        // TODO: implement test signin
//        Mockito.when(authentication.getPrincipal())
//                .thenReturn(new UserDetailsImpl(1, "admin",
//                        "admin@email.com", "admin"));

        Assertions.assertEquals("admin",
                authController.authenticateUser(login).getBody().getUsername());
    }
}
