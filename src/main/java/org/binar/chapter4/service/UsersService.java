package org.binar.chapter4.service;

import org.binar.chapter4.model.Users;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface UsersService {

    public void saveUsers(String username, String password, String email, String typeId);
    public Optional<Users> getUser(Integer userId);
    public Users getUserByUsernameEmail(String username, String email);
    public void updateEmail(String username, String newEmail);

    public Users getUsersByUsername(String username);

    public List<Users> getAllUsers();

    // Contoh pake hibernate
    public Users getUsers(String username);

    // Contoh pake PreparedStatement
    public List<String[]> getUsersPS(String typeName);

    public List<Users> getUsersByTypeName(String typeName);
}
