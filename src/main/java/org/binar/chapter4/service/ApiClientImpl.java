package org.binar.chapter4.service;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ApiClientImpl implements ApiClient{

    @Override
    public ResponseEntity<RestTemplate> getCurrentPrice() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getForEntity("https://api.coindesk.com/v1/bpi/currentprice.json", String.class);
        return new ResponseEntity<>(restTemplate, HttpStatus.OK);
    }
}
