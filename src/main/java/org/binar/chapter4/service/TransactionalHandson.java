package org.binar.chapter4.service;

import org.binar.chapter4.model.Users;
import org.binar.chapter4.repository.UsersRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Service
public class TransactionalHandson {

    private static final Logger LOG = LoggerFactory.getLogger(TransactionalHandson.class);

    @Autowired
    private UsersRepository usersRepository;

    public void testTransaction() throws SQLException {
        Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/binar", "postgres", "postgres");
        try{
            con.setAutoCommit(false);

            con.commit();
        } catch(SQLException e) {
            con.rollback();
        } finally {
            con.close();
        }
    }

    @Transactional
    public void insertNewUser(Users users) {
        usersRepository.save(users);
        float test = 5/0;
        LOG.info(String.valueOf(test));
    }

    public void insertNewUserWoTrx(Users users) {
        usersRepository.save(users);

        float test = 5/0;
        LOG.info(String.valueOf(test));
    }

}
