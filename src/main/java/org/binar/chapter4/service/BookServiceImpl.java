//package org.binar.chapter4.service;
//
//import org.binar.chapter4.model.Book;
//import org.binar.chapter4.repository.BookRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.util.ArrayList;
//import java.util.List;
//
//@Service
//public class BookServiceImpl {
//
//    @Autowired
//    BookRepository bookRepository;
//
//    public void saveBook(int bookId, String bookName, String bookAuthor, double bookCost) {
//        Book book = new Book(bookId, bookName, bookAuthor, bookCost);
//        bookRepository.save(book);
//    }
//
//    public List<Book> findBookByAuthor(String bookAuthor) {
//        return bookRepository.findBookByBookAuthor(bookAuthor);
//    }
//
//    public List<Book> findBookByBookCostLessThan(double bookCost) {
//        return bookRepository.findBookByBookCostLessThan(bookCost);
//    }
//}
