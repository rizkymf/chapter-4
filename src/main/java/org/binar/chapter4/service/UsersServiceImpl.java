package org.binar.chapter4.service;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.binar.chapter4.model.UserType;
import org.binar.chapter4.model.Users;
import org.binar.chapter4.repository.UsersRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Transactional
@Service
@RequiredArgsConstructor
public class UsersServiceImpl implements UsersService {

    private static final Logger LOG = LoggerFactory.getLogger(UsersServiceImpl.class);

    @PersistenceContext
    private EntityManager em;

    @Autowired
    @NonNull
    private UsersRepository usersRepository;

    @Autowired
    @NonNull
    private UserTypeService userTypeService;

    @CachePut
    @Override
    public void saveUsers(String username, String password, String email, String typeName) {
        Users user = new Users();
        user.setUsername(username);
        user.setPassword(password);
        user.setEmail(email);
        UserType userType = userTypeService.findByTypeName(typeName);
        if(userType != null) {
            user.setTypeId(userType);
        } else {
            userTypeService.saveUserType(typeName);
            user.setTypeId(userTypeService.findByTypeName(typeName));
        }
        usersRepository.save(user);
    }

    @Cacheable(value = "getUser")
    @Override
    public Optional<Users> getUser(Integer userId) {
        return usersRepository.findById(userId);
    }

    @Override
    public Users getUserByUsernameEmail(String username, String email) {
        return usersRepository.findByUsernameAndEmail(username, email);
    }

    @CachePut
    @Override
    public void updateEmail(String username, String newEmail) {
        Users users = usersRepository.findByUsernameAndEmail(username, "rizky@email.com");
        users.setEmail(newEmail);
        // Users 
        // user_id : 2; username : Rizky; email : rizky@email.com; password : password; type_id : 1
        usersRepository.save(users);
    }

    @Override
    public Users getUsersByUsername(String username) {
        return usersRepository.findByUsername(username);
    }

    @Cacheable(value = "getAllUsers", key = "#userId", unless = "#result.userId == 1")
    @Override
    public List<Users> getAllUsers() {
        return usersRepository.findAll();
    }

    // Contoh pake Entity Manager
    @Cacheable
    @Override
    public Users getUsers(String username) {
        String query = "select * from users where username = :username";
        Query q = em.createNativeQuery(query);
        q.setParameter("username", username);
        return (Users) q.getSingleResult();
    }

    // Query pake PreparedStatement
    @Override
    public List<String[]> getUsersPS(String typeName) {
        List<String[]> listUsers = new ArrayList<>();
        String query = "select u.username, u.email, ut.type_name from users u " +
                "join user_type ut on ut.type_id = u.type_id " +
                "where ut.type_name = ?";
        try(Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/binar", "root", "password");
            PreparedStatement ps = conn.prepareStatement(query);
        ) {
            ps.setString(1, typeName);
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                String[] hasil = new String[3];
                hasil[0] = rs.getString("username");
                hasil[1] = rs.getString("email");
                hasil[2] = rs.getString("type_name");
                listUsers.add(hasil);
            }
        } catch (SQLException sqe) {
            sqe.printStackTrace();
        }
        return listUsers;
    }

    @Cacheable(value = "getUserByTypeName")
    @Override
    public List<Users> getUsersByTypeName(String typeName) {
        List<Users> users = usersRepository.findByTypeName(typeName);
        if(users != null) {
            return users;
        } else {
            throw new NullPointerException("Null nih");
        }
    }

    public List<Integer> coba(String... test) {
        return Arrays.stream(test).map(t -> {
            Integer v = Integer.valueOf(t);
            return v;
        }).collect(Collectors.toList());
    }

    public void coba2(String ...test) {

    }

    public void coba3(String test, Integer... test2) {

    }

}
