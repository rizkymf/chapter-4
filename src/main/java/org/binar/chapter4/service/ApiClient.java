package org.binar.chapter4.service;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public interface ApiClient {
    public ResponseEntity<RestTemplate> getCurrentPrice();
}
