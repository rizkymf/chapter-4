package org.binar.chapter4.service;

import org.binar.chapter4.model.UserType;
import org.binar.chapter4.repository.UserTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserTypeServiceImpl implements UserTypeService {

    @Autowired
    private UserTypeRepository userTypeRepository;

    public UserTypeServiceImpl() {

    }

    public UserTypeServiceImpl(UserTypeRepository userTypeRepository) {
        this.userTypeRepository = userTypeRepository;
    }

    @Override
    public void saveUserType(String typeName) {
        UserType userType = new UserType();
        userType.setTypeName(typeName);
        userTypeRepository.save(userType);
    }

    @Override
    public UserType findByTypeName(String typeName) {
        return userTypeRepository.findByTypeName(typeName);
    }
}
