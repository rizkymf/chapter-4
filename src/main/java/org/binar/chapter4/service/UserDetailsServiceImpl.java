package org.binar.chapter4.service;

import org.binar.chapter4.model.UserDetailsImpl;
import org.binar.chapter4.model.Users;
import org.binar.chapter4.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    UsersRepository usersRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Users user = usersRepository.findByUsername(username);
        return UserDetailsImpl.build(user);
    }

    public UserDetails loadUserByEmail(String email){
//        Users user = usersRepository.findByUsername(username);
        Users user = usersRepository.findByEmail(email);
        return UserDetailsImpl.build(user);
    }
}
