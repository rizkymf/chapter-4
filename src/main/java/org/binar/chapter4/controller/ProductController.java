package org.binar.chapter4.controller;

import org.binar.chapter4.model.Product;
import org.binar.chapter4.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductRepository productRepository;

    @GetMapping("/find")
    public Page<Product> findProduct(
            @RequestParam(defaultValue = "", required = false) String productName,
            @RequestParam(defaultValue = "", required = false) String category,
            @RequestParam(defaultValue = "0", required = false) BigInteger priceMin,
            @RequestParam(defaultValue = "9999999999999", required = false) BigInteger priceMax,
            @RequestParam(defaultValue = "productName,asc", required = false) String[] sort,
            @RequestParam int page,
            @RequestParam int size) {
        List<Order> orders = new ArrayList<>();
        if(sort[0].contains(",")) {
            for(String sortOrder : sort) {
                String[] _sort = sortOrder.split(",");
                orders.add(new Order(Direction.fromString(_sort[1]), _sort[0]));
            }
        } else {
            orders.add(new Order(Direction.fromString(sort[1]), sort[0]));
        }
        return productRepository
                .findByProductNameContainingIgnoreCaseAndCategoryContainingAndPriceBetween
                        (productName, category, priceMin, priceMax, PageRequest.of(page, size,
                                Sort.by(orders)));
//        return productRepository.findByProductNameAndCategoryAndPrice(productName, category,
//                priceMin, priceMax, PageRequest.of(page, size));
    }

//    @GetMapping("/find")
//    public Page<Product> findProductByName(
//            @RequestParam(required = false) String productName,
//            @RequestParam int page,
//            @RequestParam int size) {
////        return productRepository.findByProductNameContainingIgnoreCase(productName,
////                PageRequest.of(page, size));
//    }

//    @GetMapping("/find")
//    public Page<Product> findProductByNameAndCategory(
//            @RequestParam(required = false) String productName,
//            @RequestParam(required = false) String category,
//            @RequestParam int page,
//            @RequestParam int size) {
//        return productRepository.findByProductNameContainingIgnoreCaseAndCategory(productName,
//                category, PageRequest.of(page, size));
//    }
}
