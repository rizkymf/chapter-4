package org.binar.chapter4.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.binar.chapter4.model.Users;
import org.binar.chapter4.repository.UsersRepository;
import org.binar.chapter4.service.ApiClient;
import org.binar.chapter4.service.UserTypeService;
import org.binar.chapter4.service.UsersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.cache.RedisCacheManagerBuilderCustomizer;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import org.springframework.data.domain.Sort.*;

/**
 * klo dijalanin di laptop sendiri tanpa merubah settingan apapun
 * url nya menjadi http://localhost:8080
 * RequestMapping dipake buat define subUrl pertama, sehingga masing2 controller punya sub url masing2
 * karena controller nya ini di Users, jadi aku kasih /users
 * sehingga url untuk di dalam controller ini harus dimulai dengan http://localhost:8080/users
 */
@Tag(name="Users", description = "API for processing with Users entity")
@RestController
@RequestMapping("/users")
public class UsersController {

    private static final Logger LOG = LoggerFactory.getLogger(UsersController.class);

    @Autowired
    private UsersService userService;

    @Autowired
    private UserTypeService userTypeService;

    @Autowired
    private ApiClient apiClient;

    @Autowired
    RedisCacheManagerBuilderCustomizer redisCacheManager;

    @Autowired
    private UsersRepository usersRepository;

    // public access

    @Operation(summary = "add user to the user table in Bioskop Database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Add user to the user table is success",
                    content = {@Content(schema = @Schema(example = "Add User Success!") )})
    })
    @PostMapping("/public/add-user")
    public String addUser(@Schema(example = "{" +
            "\"username\":\"rizkyfauzi\"," +
            "\"email\":\"rizkyfauzi@gmail.com\"," +
            "\"password\":\"password\"," +
            "\"typeName\":\"VIP\"" +
            "}") @RequestBody Map<String, Object> user) {
        // service add user
        userService.saveUsers(user.get("username").toString(), user.get("password").toString(),
                user.get("email").toString(), user.get("typeName").toString());
        return "Add User Success!";
    }

    // admin access

    /**
     * Ini method yang digunakan untuk updateUser di Table Users
     * yang perlu dimasukan adalah username dan email yang setelah di update
     * @param username String
     * @param emailAfter String
     * @return String of update status
     */
    @PutMapping(value = "/admin/update-user")
    public String updateUser(String username, String emailAfter) {
        userService.updateEmail(username, emailAfter);
        return "Update Email success";
    }

    @GetMapping(value = "/admin/get-user/{id}")
    public ResponseEntity getUser(@Parameter(description = "id of the user you want to search")
                                      @PathVariable("id") Integer id) {
        Optional<Users> users = userService.getUser(id);
        Users user = new Users();
        Map<String, Object> response = new HashMap<>();
        HttpStatus status = HttpStatus.OK;
        if(users.isPresent()) {
            user = users.get();
            response.put("name", user.getUsername());
        } else {
            status = HttpStatus.NOT_FOUND;
            response.put("message", "Error! User tidak ditemukan. . .");
        }

        LOG.info("Get user : {}", user.getUsername());
        // custom response body
        Map<String, Object> respBody = new HashMap<>();
        respBody.put("AHOY", "AHOY");

        // custom response headers
        MultiValueMap<String, String> headers = new HttpHeaders();
        headers.put("Binar", Arrays.asList("BEJ1"));
        headers.set("Keep-Alive", "timeout=6000000");

        // custom response dengan return ResponseEntity
        return new ResponseEntity<>(null, headers, status);
    }

    @GetMapping("/username-email")
    public Users getUserByUsernameAndEmail(@RequestParam("username") String username,
                                           @RequestParam("email") String email) {
        return userService.getUserByUsernameEmail(username, email);
    }

    // Customer access

    @GetMapping("/public/current-price")
    public ResponseEntity<RestTemplate> getCurrentPrice() {
        return apiClient.getCurrentPrice();
    }

    @GetMapping("/customer/kartu-nama")
    public void getKartuNama(HttpServletResponse response,
                             @RequestParam("username") String username) throws IOException, JRException {
        JasperReport sourceFileName = JasperCompileManager
                .compileReport(ResourceUtils.getFile(ResourceUtils.CLASSPATH_URL_PREFIX
                        + "kartunama2.jrxml").getAbsolutePath());

        // creating our list of beans
        List<Map<String, String>> dataList = new ArrayList<>();

        Map<String, String> data = new HashMap<>();
        Users user = userService.getUsersByUsername(username);
        data.put("username", user.getUsername());
        data.put("email", user.getEmail());
        data.put("typeName", user.getTypeId().getTypeName());
        data.put("dateNow", String.valueOf(new Date()));
        data.put("cobaField", "Ini hanya cobaan");
        dataList.add(data);

        // creating datasource from bean list
        JRBeanCollectionDataSource beanColDataSource = new JRBeanCollectionDataSource(dataList);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("createdBy", "Rizky");
        JasperPrint jasperPrint = JasperFillManager.fillReport(sourceFileName, parameters, beanColDataSource);

        response.setContentType("application/pdf");
        response.addHeader("Content-Disposition", "inline; filename=kartunama.pdf;");

        JasperExportManager.exportReportToPdfStream(jasperPrint, response.getOutputStream());
    }

    @GetMapping("/getCache")
    public void testGetCache() {

    }

    // TODO: implement upload file
    @PostMapping("/upload")
    public void upload(@RequestBody MultipartFile... file) {

    }

    @GetMapping("/users")
    public Page<Users> listUsersPagination(
            @RequestParam("page") int page,
            @RequestParam("size") int size) {
//        List<Sort.Order> orders = new ArrayList<Order>();
//        if (sort[0].contains(",")) {
//            // will sort more than 2 fields
//            // sortOrder="field, direction"
//            for (String sortOrder : sort) {
//                String[] _sort = sortOrder.split(",");
//                orders.add(new Order());
//                orders.add(new Order(getSortDirection(_sort[1]), _sort[0]));
//            }
//        } else {
//            // sort=[field, direction]
//            orders.add(new Order(getSortDirection(sort[1]), sort[0]));
//        }
        return usersRepository.findUsersPagination(PageRequest.of(page, size));
    }
}
