package org.binar.chapter4.config;

import org.binar.chapter4.scheduler.AnyJob;
import org.binar.chapter4.scheduler.LoadUserJob;
import org.quartz.JobDetail;
import org.quartz.Trigger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.*;

@Configuration
public class QuartzScheduler {

    private final ApplicationContext applicationContext;

    @Autowired
    public QuartzScheduler(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Bean
    public SpringBeanJobFactory springBeanJobFactory() {
        SpringBeanJobFactory jobFactory = new SpringBeanJobFactory();
        jobFactory.setApplicationContext(this.applicationContext);
        return jobFactory;
    }

    @Bean
    public SchedulerFactoryBean scheduler() {
        SchedulerFactoryBean schedulerFactory = new SchedulerFactoryBean();
        schedulerFactory.setJobFactory(springBeanJobFactory());
        JobDetail[] jobs = {
                anyJobDetail().getObject(),         // Job 1 detail
                loadUserJobDetail().getObject()     // Job 2 detail
        };
        Trigger[] triggers = {
                anyJobTrigger().getObject(),        // Job 1 trigger
                loadUserJobTrigger().getObject()    // Job 2 trigger
        };
        schedulerFactory.setJobDetails(jobs);
        schedulerFactory.setTriggers(triggers);
        return schedulerFactory;
    }

    // Job 1 dengan nama job "AnyJob"
    @Bean
    public JobDetailFactoryBean anyJobDetail() {
        JobDetailFactoryBean jobDetail = new JobDetailFactoryBean();
        jobDetail.setJobClass(AnyJob.class);
        jobDetail.setName("HandsonScheduleJob");
        jobDetail.setDescription("Job Hands on for quartz scheduler");
        jobDetail.setDurability(true);
        return jobDetail;
    }

    @Bean
    public CronTriggerFactoryBean anyJobTrigger() {
        CronTriggerFactoryBean cronTrigger = new CronTriggerFactoryBean();
        cronTrigger.setJobDetail(anyJobDetail().getObject());
        cronTrigger.setName("HandsonScheduleTrigger");
        // 0 0 11,13,15,17 * * MON-FRI
        cronTrigger.setCronExpression("0/10 * * ? * * *");
        return cronTrigger;
    }

    // Job 2 dengan nama job LoadUserJob
    @Bean
    public JobDetailFactoryBean loadUserJobDetail() {
        JobDetailFactoryBean jobDetail = new JobDetailFactoryBean();
        jobDetail.setJobClass(LoadUserJob.class);
        jobDetail.setName("Job Load User");
        jobDetail.setDescription("Job for Load Users");
        jobDetail.setDurability(true);
        return jobDetail;
    }

    @Bean
    public CronTriggerFactoryBean loadUserJobTrigger() {
        CronTriggerFactoryBean cronTrigger = new CronTriggerFactoryBean();
        cronTrigger.setJobDetail(loadUserJobDetail().getObject());
        cronTrigger.setName("Load User Job Trigger");
        // 0 0 11,13,15,17 * * MON-FRI
        cronTrigger.setCronExpression("0/10 * * ? * * *");
        return cronTrigger;
    }
}
