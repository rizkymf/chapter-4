package org.binar.chapter4.repository;

import org.binar.chapter4.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    @Query("select p from Product p where lower(p.productName) like lower(:productName) and " +
            "p.category = :category and p.price between :priceMin and :priceMax")
    Page<Product> findByProductNameAndCategoryAndPrice(@Param("productName") String productName,
            @Param("category") String category, @Param("priceMin") BigInteger priceMin,
            @Param("priceMax") BigInteger priceMax, Pageable pageable);

    Page<Product> findByProductNameContainingIgnoreCaseAndCategoryContainingAndPriceBetween(
            String productName, String category, BigInteger priceMin,
            BigInteger priceMax, Pageable pageable);

    Page<Product> findByProductNameContainingIgnoreCaseAndCategory(String productName,
                                                                   String category,
                                                                   Pageable pageable);
}
