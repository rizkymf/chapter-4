//package org.binar.chapter4.repository;
//
//import org.binar.chapter4.model.Book;
//import org.springframework.data.mongodb.repository.MongoRepository;
//import org.springframework.data.mongodb.repository.Query;
//import org.springframework.stereotype.Repository;
//
//import java.util.List;
//
//@Repository
//public interface BookRepository extends MongoRepository<Book, String> {
//
//    @Query("{bookAuthor: ?0}")
//    public List<Book> findBookByBookAuthor(String bookAuthor);
//
//    @Query("{bookCost: {$lt: ?0}}")
//    public List<Book> findBookByBookCostLessThan(double bookCost);
//}
