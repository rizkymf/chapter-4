package org.binar.chapter4.repository;

import org.binar.chapter4.enumeration.ERole;
import org.binar.chapter4.model.Roles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Roles, Integer> {

    Optional<Roles> findByName(ERole name);
}
