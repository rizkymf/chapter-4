package org.binar.chapter4.repository;

import org.binar.chapter4.model.UserType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserTypeRepository extends JpaRepository<UserType, Integer> {

    public UserType findByTypeName(String name);

}
