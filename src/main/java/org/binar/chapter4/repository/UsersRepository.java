package org.binar.chapter4.repository;

import org.binar.chapter4.model.Users;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsersRepository extends JpaRepository<Users, Integer> {

    @Query("select u from users u where u.username = :username and u.email = :email")
    public Users findByUsernameAndEmail(@Param("username") String userName, @Param("email") String email);

    @Query("select u from users u where u.email = :email")
    public Users findByEmail(@Param("email") String email);

    public Users findByUsername(String username);
    Boolean existsByUsername(String username);
    Boolean existsByEmail(String email);

    @Query(value = "select * from users u " +
            "join user_type ut on ut.type_id = u.type_id " +
            "where ut.type_name = :typeName", nativeQuery = true)
    public List<Users> findByTypeName(@Param("typeName") String typeName);

    Page<Users> findAll(Pageable pageable);

    @Query(value = "select * from users u", nativeQuery = true)
    Page<Users> findUsersPagination(Pageable pageable);
}
