//package org.binar.chapter4.producer;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.kafka.core.KafkaTemplate;
//import org.springframework.stereotype.Service;
//
//@Service
//public class Producer {
//
//    @Autowired
//    KafkaTemplate<String, String> kafkaTemplate;
//
//    public void produceMsg(String payload) {
//        kafkaTemplate.send("handsonBEJ", payload);
//    }
//}
