//package org.binar.chapter4.producer;
//
//import org.apache.kafka.clients.producer.KafkaProducer;
//import org.apache.kafka.clients.producer.ProducerConfig;
//import org.apache.kafka.common.serialization.StringDeserializer;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.kafka.core.DefaultKafkaProducerFactory;
//import org.springframework.kafka.core.KafkaTemplate;
//
//import java.util.HashMap;
//import java.util.Map;
//
//@Configuration
//public class ProducerCfg {
//
//    @Bean
//    public Map<String, Object> producerConfig() {
//        Map<String, Object> properties = new HashMap<>();
//        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
//        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringDeserializer.class);
//        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringDeserializer.class);
//        return properties;
//    }
//
//    @Bean
//    public DefaultKafkaProducerFactory<String, String> producerFactory() {
//        DefaultKafkaProducerFactory<String, String> pf = new DefaultKafkaProducerFactory<String, String>(producerConfig());
//        pf.setTransactionIdPrefix("mytrans_");
//        return pf;
//    }
//
//    @Bean
//    public KafkaTemplate<String, String> kafkaTemplate() {
//        return new KafkaTemplate<String, String>(producerFactory());
//    }
//
//    @Bean
//    public KafkaProducer<String, String> kafkaProducer(){
//        KafkaProducer<String, String> kafkaProducer = new KafkaProducer<String, String>(producerConfig());
//
//        kafkaProducer.initTransactions();
//
//        return kafkaProducer;
//    }
//
////    @Bean
////    public Producer producer() {
////        return new Producer();
////    }
//}
