package org.binar.chapter4.model;

import lombok.Data;

@Data
public class UploadResponse {
    String message;
    String[] url;
}
