package org.binar.chapter4.model;

import lombok.Getter;
import lombok.Setter;
import org.binar.chapter4.enumeration.ERole;

import javax.persistence.*;
import java.io.Serializable;

@Setter
@Getter
@Entity
@Table(name = "roles")
public class Roles implements Serializable {

    private static final long serialVersionUID = 78796483L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer roleId;

    @Enumerated(EnumType.STRING)
    @Column(length = 20)
    private ERole name;

    public Roles() {
        // This method is empty because it is needed when object is ready
    }

    public Roles(Integer roleId, ERole name) {
        this.roleId = roleId;
        this.name = name;
    }
}
