package org.binar.chapter4.model;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;

@Data
@Entity
public class Images {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id")
    private Long id;

    @Column(name = "image_name")
    private String imageName;

    @Lob
    @Column(name = "image_file")
    private byte[] imageFile;

    public Images() {

    }

    public Images(String imageName, byte[] imageFile) {
        this.imageName = imageName;
        this.imageFile = imageFile;
    }
}
