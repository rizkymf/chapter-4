package org.binar.chapter4.model;

import lombok.Data;

import javax.persistence.*;
import java.math.BigInteger;

@Data
@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long productId;

    @Column
    private String productName;

    @Column
    private String category;

    @Column
    private BigInteger price;
}
