package org.binar.chapter4.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Setter
@Getter
@Embeddable
public class BookId implements Serializable {

    // Composite key yg terdiri dari judulBuku, edisi, dan penulis
    private String judulBuku;
    private String edisi;
    private String penulis;

}
