package org.binar.chapter4.model;

import com.mongodb.lang.NonNull;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
public class Book {

    // embeddable
//    @EmbeddedId
//    private BookId bookId;
//
//    private String harga;
//    private String quantity;

    @Id
    private String id;

    @NonNull
    private Integer bookId;

    @NonNull
    private String bookName;

    @NonNull
    private String bookAuthor;

    @NonNull
    private double bookCost;

    public Book() {

    }

    public Book(Integer bookId, String bookName, String bookAuthor, double bookCost) {
        this.bookId = bookId;
        this.bookName = bookName;
        this.bookAuthor = bookAuthor;
        this.bookCost = bookCost;
    }
}
